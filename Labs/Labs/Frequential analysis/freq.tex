\documentclass[a4paper,11pt]{article}

%\usepackage[showframe]{geometry} %use this if you want to check margins
\usepackage{geometry}
\geometry{centering,total={160mm,250mm},includeheadfoot}

%auxiliairy files
\input{header.tex} %usepackage and configurations
\input{cmds.tex} %user defined commands

%title
\newcommand{\doctitle}{Sécurité des réseaux - Laboratoire (SECRL5)}
\newcommand{\docsubtitle}{Homework 1}
\newcommand{\tdyear}{2019 - 2020}
\author{R. Absil}

%language input
\lstset{language=c++,
		morekeywords={constexpr,nullptr}}

%language={[x86masm]Assembler}
%language=c++, morekeywords={constexpr,nullptr}
%language=Java

\begin{document}
\pagestyle{fancy} %displays custom headers and footers

\maketitle

The objective of this homework is to have students to run frequential analysis attacks on the Cesar and Vigenere ciphers. This document first introduces the reader to the concept of confidentiality, and, in particular, cryptography, then illustrates two trivial cipher algorithms, namely the cipher of Cesar and of Vigenere. Then, in each case, we explain how to exploit the vulnerabilities of these ciphers in order to recover ciphered text without the cryptographic key.

Finally, we detail the expectations regarding the work to submit. This homework has to be made by groups of one or two students, and the deadline is set on November 17, 23h55.

\tableofcontents

\section{Introduction}

There are several key "features" that any security system must implement\footnote{One could argue that security is not a feature. While this is true that a system must be designed to be secured instead of being iteratively patched to have this quality, we will still use this abuse of language.}, one of them is confidentiality.

Confidentiality means that sensitive data are of no use to unauthorised users. It doesn't mean that these data cannot be accessed by malicious users, it means that even if an attacker actually reads these data, he can recover no useful information in a \emph{reasonable}\footnote{Reasonable means that the number of elementary operations an algorithm has to perform in the worst case in order to read these data is not bounded by a polynomial. For instance, if a password policy enforces users to choose alphanumeric lowercase passwords of length 8, there are $26^8$ such passwords. Consequently, if an attacker wants to check every possible password, he has to make that many tries. Generalising this approach for passwords of length $n$, an attacker has to guess (or brute force) among $26^n$ possibilities. That function is not bounded by a polynomial.} amount of time.

Confidentiality can be implemented in two approaches:
\begin{enumerate}
\item hide sensitive data (steganography),
\item make sensitive data uncomprehensible (cryptography),
\end{enumerate}
for unauthorised users. We will discard the first approach according to one of Kerckoff's principles: "secrecy is a weakness", or, in other words, the security of a system must rely on the access codes only, and not on the system itself. 

In the case of cryptography, the access codes are called \emph{cryptographic keys}: they are the only piece of information needed to encode or decode sensitive data, usually a message. An algorithm able to encode or decode information with the help of a key is called a \emph{cipher algorithm}.

Cipher algorithms are usually categorised into two main sets :
\begin{enumerate}
\item symmetric ciphers : the same key allows to both encode and decode information,
\item public key cryptography : a key is used to encode, and another one to decode\footnote{In the case of confidentiality, the key used to encode information is called the \emph{public key}, and the one use to decode is called the \emph{private key}. Only the private key has to be kept secret. It is not possible to deduce one key from another in a reasonable amount of time.}.\\
\end{enumerate}

In the case of this homework, we are interested in trivial symmetric cipher algorithms : the cipher of Cesar and the cipher of Vigenere. Both of them use a single key to encode sensitive data, called the \emph{plain text}, into an unreadable form, called the \emph{ciphered text}.

\section{The cipher of Cesar}

The cipher of Cesar is famous for being used by military officers of the Roman Empire during the antiquity. The point was to secretly transmit military (and obviously sensitive) orders with the help of horse riding messengers. Should the messenger be captured and potentially tortured in various fashionable ways, the secret orders were safe since the adversary had no copy of the key and could consequently not decipher the orders.

The principle of this algorithm is actually quite simple: if a plain text uses an alphabet of size $n$, pick an integer number $k$ between $1$ and $n$. That integer $k$ is the \emph{cryptographic key} of the system : it is the only secret that needs to be protected. We use the key $k$ on the plain text by shifting the input alphabet of $k$ characters in order to produce the ciphered text.

For instance, if we use the key $k=3$, we will shift the character 'a' by $3$, and it will become a 'd'. In the same way, any 'b' will become 'e', etc. This technique is illustrated on Figure~\ref{fig:cesar}. On a concrete example, the string "Security course" is ciphered as "wigyvmxc gsyvwi" with a key $k=3$.

\begin{figure}[!htp]
\begin{center}
\begin{tikzpicture}
\draw[dotted, thick] (0,0) -- (-1,0) --(-1,-1) -- (0,-1);
\draw[dotted, thick] (1,0) -- (0,0) --(0,-1) -- (1,-1);
\draw[dotted, thick] (2,0) -- (1,0) --(1,-1) -- (2,-1);
\node at (-0.5,-0.5) {X};
\node at (0.5,-0.5) {Y};
\node at (1.5,-0.5) {Z};

\draw[blue!30,fill=blue!30] (3,-1) rectangle (4,0);
\draw[blue!30,fill=blue!30] (6,-5) rectangle (7,-4);

\node at (8.5,-0.5) {$\dots$};
\node at (1.5,-4.5) {$\dots$};
\node at (10,-1.5) {\Large $k=3$};

\foreach \i/\j in {2/A,3/B,4/C,5/D,6/E,7/F}
{
	\draw (\i,-1) rectangle (\i+1,0);
	\node at (\i+0.5,-0.5) {\j};
	\draw (\i,-5) rectangle (\i+1,-4);
	\node at (\i+0.5,-4.5) {\j};
}

\foreach \i in {2,3,4}
{
	\draw[thick,->,>=stealth] (\i+0.5,-1) to[out=-90,in=90] (\i+3.5,-4);
}

\foreach \i in {-1,0,1,5,6,7}
{
	\draw[dotted,thick,->,>=stealth] (\i+0.5,-1) to[out=-90,in=90] (\i+3.5,-4);
}

\draw[dotted, thick] (8,-4) -- (9,-4) --(9,-5) -- (8,-5);
\draw[dotted, thick] (9,-4) -- (10,-4) --(10,-5) -- (9,-5);
\draw[dotted, thick] (10,-4) -- (11,-4) --(11,-5) -- (10,-5);
\node at (8.5,-4.5) {G};
\node at (9.5,-4.5) {H};
\node at (10.5,-4.5) {I};
\end{tikzpicture}
\caption{Cipher of Cesar}
\label{fig:cesar}
\end{center}
\end{figure}

The cipher of Cesar is symmetric because the same key allows to both encode and decode. Indeed, if we use the key $k$ to encode our text, we shift the input alphabet by $k$ characters. Consequently, we can recover the plain text from the ciphered text by shifting the alphabet by $-k$ characters.

\subsection{Cryptanalysis}

The cipher of Cesar suffers two significative drawbacks. The first one comes from the reduced size of the key space: on a alphabet of size $n$, there are only $n-1$ possible keys.

Consequently, an attacker could try every possible key on a ciphered text. Such approach, called a \emph{brute force attack}, requires $n$ tries. Since $n$ is bounded by a polynomial, cracking in this way is considered easy, and the cipher is weak.

Another problem with the cipher of Cesar is that is preserves the frequencies of letters, both in the plain and ciphered text. For instance, the most frequent letter in English is 'e'. Then, if we cipher an English text, the most frequent character in the ciphered text most likely is the 'e' from the plain text.

A simple approach, called \emph{frequential analysis}, simply consists in computing frequencies in the ciphered text, and aligning the distribution curve from the ciphered to the theoretical one of an English text\footnote{Note that, in some cases, that will not mean align the most frequent letter from the ciphered text to the letter 'e' and deduce the key. Use of the $\chi^2$ test is advised.} with statistical analysis. From there, the key can be deduced and the text deciphered.

\section{The cipher of Vigenere}

The cipher of Vigenere aims to palliate the two drawbacks of the cipher of Cesar. For that purpose, a key is chosen as a $n$-uple of numbers $k=(k_1,k_2,\dots,k_n)$. We say that the \emph{length} of such a key is $n$. Then, similarly to Cesar, we encode the plain text with the help of alphabet shifting.

For instance, with the key $(8,5,3,14)$, the first letter is shifted from $8$, the second one from $5$, the third one from $3$, the fourth one from $14$, the fifth one from $8$, etc. An arbitrary large input text is then encoded by cycling different shifts with the key. Sometimes, instead of representing the key with an $n$-uple, it is encoded as a word. In the case of $k=(8,5,3,14)$, we can represent $k$ with the word "HECM", since 'H' is the eight length of the alphabet, 'E' the fifth, etc. 

Such principle is illustrated on Figure~\ref{fig:vig}. The string "security course" ciphered with the key "info" leads to the ciphered text "arhizvym kbzfar".

\begin{figure}[!htp]
  \begin{center}
    \begin{tt}
      \begin{tabular}{r|r}
       & \color{red}Plain text\\
       \color{blue}Key & \color{red}A B C D E F G H I J K L M N O P Q R S T U V W X Y Z\\
	\hline
       \color{blue}I & \color{black}I J K L M N O P Q R S T U V W X Y Z A B C D E F G H\\
       \color{blue}N & \color{black}N O P Q R S T U V W X Y Z A B C D E F G H I J K L M\\
       \color{blue}F & \color{black}F G H I J K L M N O P Q R S T U V W X Y Z A B C D E\\
       \color{blue}O & \color{black}O P Q R S T U V W X Y Z A B C D E F G H I J K L M N\\
    \end{tabular}
    \end{tt}
\\[2em]
	\begin{tt}
	\begin{tabular}{r|l}
     \color{red}Plain text
     &\color{red}SECURITY COURSE\\
      \color{blue}Key &\color{blue}INFOINFO INFOIN \\
      \color{black}Ciphered text&\color{black}ARHIZVYM KBZFAR\\
     \end{tabular}
     \end{tt}
  \end{center}
  \caption{The cipher of Vigenere}
  \label{fig:vig}
\end{figure}

We can already conclude that a brute force attack is unfeasible in this context, since if we have an alphabet of size $a$, such an approach would require $a^n$ tries to find a random key. 

On the other hand, we notice that the cipher of Cesar is just a particular case of the cipher of Vigenere: with keys of length $1$. This is exactly why a direct frequential analysis approach will not lead to conclusive results with Vigenere, since the same characters from the plain text are most likely not shifted in the same way.

\subsection{Cryptanalysis}

However, despite the improvements made regarding the cipher of Cesar to prevent both a brute force attack and a direct frequential analysis, the cipher is Vigenere is still weak.

To prove this statement, let us first assume we know the length $l$ of the key $k$ used to cipher some plain text. If such a key is used, the first character of the plain text is encoded by the first character of the key, as well as the characters of index\footnote{We consider here that the indexes of characters start from $1$, and not from $0$.} $l+1$, $2l+1$, $3l+1$, etc. Consequently, we could create a subtext composed of the characters of index $1$, $l+1$, $2l+1$, $3l+1$, etc., and run a direct frequential analysis in the way it is implemented in the cipher of Cesar to decode part of the ciphered text. Performing the same operation with the texts composed of the characters of index $2$, $l+2$, $2l+2$, $3l+2$, etc., along with the ones for the others components of the key, will completely break the cipher. As a conclusion, $l$ "Cesar" frequential analyses on a Vigenere ciphered text will recover the plain text.

With this construction, we reduced the problem of breaking the Vigenere cipher without the key to just find the length of that key. 

One way to find it out is to study repetitions in the ciphered text, as illustrated on Figure~\ref{fig:repet}. Note that on this figure, for the sake of simplification, we discarded blank spaces. Here, we study repetitions of substrings of length $3$.

\begin{figure}[!htp]
\begin{center}
\begin{small}
\begin{tt}
\begin{tabular}{r|l}
       \color{red}Plain text
       &\color{red}MY TALKS ARE AWLAYS CLEAR AND MY TWENTY ONE STUDENTS HAVE NO FEAR\\
       \color{blue}Key &\color{blue}IN FOINF OIN FOINFO INFOI NFO IN FOINFO INF OINFOIN FOIN FO INFOI\\
       \color{black}Ciphered text& \textcolor{purple}{UL Y}OTXX OZR FKTNDG KY\textcolor{cyan}{JOZ} NSR \textcolor{purple}{UL Y}KMAYM WAJ GBHISVGX VIIJ BW S\textcolor{cyan}{JOZ}\\
\end{tabular}
\end{tt}
\end{small}   
    \caption{Repetitions in ciphered text}
    \label{fig:repet}
\end{center}
\end{figure}

Such repetitions can happen for two reasons:
\begin{enumerate}
\item randomness: on a sufficiently large input text, repetitions will appear,
\item congruence: it is possible that two identical parts of an input text are encoded by the same part of the key.
\end{enumerate}

We are interested in the second case, since the first one provides no useful information. However, we can conclude that if two non-random repetitions are separated by $q$ characters, then the length of the key divides $q$, since it is "recopied" an integer number of times between the two substrings. This situation is illustrated on Figure~\ref{fig:divide}.

\newcommand{\rack}[3]{
	\draw (#1,#2) -- (#1+#3,#2) node[above, midway , fill=white] {Key};
	\draw (#1,#2) -- (#1,#2 + 0.25);
	\draw (#1+#3,#2) -- (#1+#3,#2 + 0.25);	
}

\begin{figure}[!htp]
\begin{center}
\begin{tikzpicture}[scale=.75]
\draw (0,0) -- (15, 0);

\foreach \i in {0, 1, 2, 3}
{
	\draw (\i,0) -- (\i,1);
	\draw (\i+12,0) -- (\i+12,1);		
}

\node at (0.5,0.5) {J};
\node at (1.5,0.5) {O};
\node at (2.5,0.5) {Z};
\node at (12.5,0.5) {J};
\node at (13.5,0.5) {O};
\node at (14.5,0.5) {Z};

\draw[<->, very thick] (0,-0.5) -- (12, -0.5) node[below, midway] {Distance between the JOZ};

\draw[dotted] (0,-0.5) -- (0,-5.5);
\draw[dotted] (1,-0.5) -- (1,-5.5);
\draw[dotted] (2,-0.5) -- (2,-5.5);
\draw[dotted] (3,-0.5) -- (3,-5.5);
\draw[dotted] (12,-0.5) -- (12,-5.5);
\draw[dotted] (13,-0.5) -- (13,-5.5);
\draw[dotted] (14,-0.5) -- (14,-5.5);
\draw[dotted] (15,-0.5) -- (15,-5.5);

\rack{0}{-2}{4}
\rack{4}{-2}{4}
\rack{8}{-2}{4}
\rack{12}{-2}{4}

\rack{-1}{-3}{4}
\rack{3}{-3}{4}
\rack{7}{-3}{4}
\rack{11}{-3}{4}

\rack{1}{-4}{4}
\rack{5}{-4}{4}
\rack{9}{-4}{4}
\rack{13}{-4}{4}

\rack{0}{-5.5}{6}
\rack{6}{-5.5}{6}
\rack{12}{-5.5}{6}
\end{tikzpicture}
\caption{The length of the key divides the distance between repetitions}
\label{fig:divide}
\end{center}
\end{figure}

Moreover, the length of the key does not only divide the distance between two arbitrary non-random repetitions, it divides \emph{all of them}. Hence, a common divider\footnote{For instance, the greatest common divider.} of all these distances is most likely\footnote{Note that a divider actually in common with all distances between repetitions is unlikely to even exist, for instance because some of them are random and some distances will be prime with each others. In that case, a "common enough" divider should be a reasonable choice.} a good choice for the length of the key.

On the above example, we have
\begin{itemize}
\item distance between the {\color{purple}ULY} : {\color{purple}24},
\item distance between the {\color{cyan}JOZ} : {\color{cyan}32},
\item $\textrm{gcd}(24,32) = 8$,
\end{itemize}
and choosing $8$ as a length of the key works, even tough it is not the minimum length.

This way of finding the length of the key is called the Kasiski method. While easy to understand, there exist other ways of finding it out, for instance the Friedman method, using the coincidence index\footnote{\url{https://en.wikipedia.org/wiki/Index_of_coincidence} - Last accessed on \today.}.

%Note that, again, some statistical analysis will be required to choose both the length of the key, and then to align the distributions curves in the same way it should be done with the cipher of Cesar. Similarly, a cryptanalyst has no \emph{a priori} no way of knowing whether some repetitions are random or not. Again, statistical analysis will provide that answer.

\section{Homework}

You must provide a set of scripts\footnote{That is, you don't have to implement a fully modular code by following well studied design patterns.} in order to
\begin{itemize}
\item cipher plain text with the cipher of Cesar and a key,
\item cipher plain text with the cipher of Vigenere and a key,
\item decipher ciphered text with the cipher of Cesar and a key,
\item decipher ciphered text with the cipher of Vigenere and a key,
\item decipher ciphered text with the cipher of Cesar without a key (that is, with the help of frequential analysis),
\item decipher ciphered text with the cipher of Vigenere without a key (that is, with the help of frequential analysis).
\end{itemize}

Note that you will only be graded on the two last parts. For this purpose, you can make some preprocessing on the plain text you input into your system, such as removing blank spaces, tabulations, punctuation, end of line characters, etc. You can as well remove any form of dyacritics, for instance by switching characters such as 'é' by 'e'.

Note that in practice, frequential and statistical analysis behaves well with sufficiently large inputs. Hence, it is probably wise to input large text files, for instance found on \url{https://www.gutenberg.org/browse/languages/en}.

The choice of language is left to your discretion (but that choice is your responsibility), as long as the required libraries are in updated Debian packages\footnote{Obviously, your project must be able to run on such a distribution, or a similar one.}. Should you use custom libraries, their code \emph{must} be open-source. For deployment, you must provide a \texttt{README} file explaining how to use your project (that is, the set of commands necessary to build the scripts\footnote{That includes compiling source code, installing missing dependencies and configure what needs to be.} and run the attack).

The project has to be made by groups of one or two students (one submission per group). Your \texttt{README} file must mention the student numbers of each member of the group. The project has to be submitted on the Webcampus platform on March 03, 23h55.

\end{document}

