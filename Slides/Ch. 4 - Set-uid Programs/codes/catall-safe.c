#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int main(int c, char* args[])
{	
	if(c < 2)
	{
		printf("You must provide a file name\n");
		return 1;
	}

	char * cmd[] = {"/bin/cat", args[1], 0};

	setuid(0);
	execve(cmd[0], cmd, 0);
	setuid(getuid());
}
