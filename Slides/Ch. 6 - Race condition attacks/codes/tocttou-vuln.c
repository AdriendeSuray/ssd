#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main()
{    
    char buffer[70];    
    //printf("What do you want to write?\n");
    scanf("%60s", buffer);   
    
    char * filename = "/tmp/XYZ";
    if(! access(filename, W_OK))
    {
        FILE * file = fopen(filename, "a+"); //open for reading and appending
        
        fwrite("\n", sizeof(char), 1, file);
        fwrite(buffer, sizeof(char), strlen(buffer), file);
        fclose(file);
    }
    else
        printf("Access denied\n");        
}
