#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>

int main()
{
    struct stat st;
    int fd = open("./zzz", O_RDONLY);
    fstat(fd, &st);
    void * map = mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    
    int mfd = open("/proc/self/mem", O_RDWR); //open the process's memory
    lseek(mfd, (off_t) map + 5, SEEK_SET); //move to byte 5
    
    const char* stuff = "stuff";
    write(mfd, stuff, strlen(stuff)); //writes to memory (triggers COW)
    
    //checks whether the write is successful
    char buffer[30];
    memcpy(buffer, map, 2 * 5 + strlen(stuff));
    printf("After write  : %s\n", buffer);
    
    //checks the content after madvise
    madvise(map, st.st_size, MADV_DONTNEED);
    memcpy(buffer, map, 2 * 5 + strlen(stuff));
    printf("After madvise: %s\n", buffer);
    
    close(fd);
    close(mfd);
}
