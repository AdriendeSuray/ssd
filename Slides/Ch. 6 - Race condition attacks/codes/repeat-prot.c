#include <stdio.h>
#include <fcntl.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

void write_to_file(int fd)
{
    //do stuff   
}

bool repeat_check(unsigned n, int fd[])
{
    for(unsigned i = 0; i < n ; i++)
    {
        if(access("/tmp/XYZ", O_RDWR))
            return false;
        else
            fd[i] = open("/tmp/XYZ", O_RDWR);
    }
    return true;
}

bool race_check(unsigned n, const int const fd[])
{
    struct stat stats[n];
    for(unsigned i = 0; i < n; i++)
        fstat(fd[i], &(stats[i]));
    
    bool race = false;
    for(unsigned i = 1; i < n && !race; i++)        
        race = stats[0].st_ino != stats[i].st_ino;
    
    return race;
}

int main()
{
    unsigned n_checks = 3;
    
    int fd[n_checks];
    if(! repeat_check(n_checks, fd))
    {
        fprintf(stderr, "Permission denied\n");
        return 1;
    }        
    
    if(race_check(n_checks, fd))
        fprintf(stderr, "Race condition detected\n");
    else
        write_to_file(fd[0]); //do stuff
}
