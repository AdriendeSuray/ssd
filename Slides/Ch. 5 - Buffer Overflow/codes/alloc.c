int x = 100; //.data

int main()
{
	int a = 2; //stack
	float b = 2.5; //stack
	
	static int y; //.bss
	
	int *pt = (int*) malloc(2 * sizeof(int)); //pt on stack, *pt on heap
	pt[0] = 5; //on heap
	pt[1] = 6; //on heap
	
	free(pt);
}