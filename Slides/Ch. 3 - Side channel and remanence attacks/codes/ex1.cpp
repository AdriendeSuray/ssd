#include <iostream>

using namespace std;

int main()
{
	int i = 0; //sensitive

	while(i != -1)
	{		
		cout << "Type -1 to exit, -2 to print i, sth else to change value" << endl;
		int j;
		cin >> j;
		if(j == -2)
			cout << "i = " << i << endl;
		else
			i = j;
	}
}
