#include <iostream>
#include <limits>
#include <stdlib.h>

using namespace std;

int main()
{
	static unsigned mask = rand() % 100 + 1;

	int i = 0;
	int canari = i ^ mask;

	while(i != -1)
	{		
		if(i != (canari ^ mask))
		{
			cout << "CHEATER" << endl;
			break;
		}

		cout << "Type -1 to exit, -2 to print i, sth else to change value" << endl;		
		int j;
		cin >> j;
		if(j == -2)
			cout << "i = " << i << endl;
		else
		{
			i = j;
			canari = j ^ mask;
		}
	}
}
